package com.cooper.assessments.scripts

import org.springframework.web.reactive.function.client.WebClient
import java.io.File

val client = WebClient.create("https://coopergis.integritygis.com")

fun main(args: Array<String>) {
	// step 1
	fetchFromApi()

	// step 2
	extractNames()
}

private fun extractNames() {
	val out = File("assets/names/names.txt")
	('a'..'z')
			.map { c ->
				File("names-$c.txt").readText()
			}
			.flatMap {
				"\\{\"REALDATA_NAME\":\"([^\"]+)\"\\}".toRegex()
						.findAll(it)
						.toSet()
						.map { it.groupValues[1] }
						.minus("REALDATA_NAME")
			}
			.toSet()
			.map { out.appendText("$it\n") }
}

private fun fetchFromApi() {
	('a'..'z').forEach { c ->
		val file = File("assets/names/byLetter/names-$c.txt")
		if (file.exists())
			return // don't fetch again

		val response = client.get()
				.uri {
					it
							.path("/Geocortex/Essentials/REST/sites/Cooper_County_MO/map/mapservices/2/rest/services/x/MapServer/12/query")
							.queryParam("f", "json")
							.queryParam("where", "REALDATA_NAME LIKE UPPER ('$c%')")
							.queryParam("returnGeometry", false)
							.queryParam("spatialRel", "esriSpatialRelIntersects")
							.queryParam("outFields", "REALDATA_NAME")
							.queryParam("returnDistinctValues", true)
							.build()
				}
				.exchange()
				.block()
				?.bodyToMono(String::class.java)
				?.block()

		if (response != null) {
			file.writeText(response)
		}
	}
}