package com.cooper.assessments.scripts

import com.cooper.assessments.Family
import com.cooper.assessments.Person
import java.io.File

val lineBlacklist = listOf("INC", "INC.", "LLC", "ASSOC", "ASSN.", "CHURCH", "UNIVERSITY",
		"UNKNOWN", "LP", "CONGREGATION", "CO", "CO.", "STATES", "BANK", "CEMETERY", "LLLP", "BOONSLICK",
		"CORP", "GROUP", "PROPERTIES", "CORPORATION", "OTTERVILLE", "SCHOOL", "DIST", "DISTRICT",
		"BOONVILLE", "CONSERVATION", "BLACKWATER", "UNCLAIMED", "CEMETARY", "ASSOC.", "COMPANY", "TRUST", "LC",
		"VILLAGE", "TRUSTEES", "PRAIRIE", "METHODIST", "COUNTY", "FINANCE", "PALESTINE", "CENTER", "MO", "MISSOURI",
		"ESTATE", "LTD", "EVANGELICAL", "BUNCETON", "MONITEAU", "DEVELOPMENT", "PARTNERSHIP", "TRSUT", "LAMINE", "ASSO",
		"M.F.A.", "M.K.T", "CITY", "COOP", "CO-OP", "PUBLIC", "FIRE", "LLP", "FARM", "SOCIETY", "FARMS", "GRAVEYARD", "ENERGY",
		"INVESTMENTS", "COLUMBIA")

val names = File("assets/names/names.txt")
		.readLines()

val oddballs = mutableListOf<String>()

val polished = mutableSetOf<Person>()
val families = mutableSetOf<Family>()

fun main(args: Array<String>) {
	standardNames()

//	nonstandardNames()
	val namesOut = File("assets/names/polished/names.txt")
	namesOut.writeText("")

	polished
			.sortedBy { it.firstName }
			.sortedBy { it.lastName }
			.forEach { namesOut.appendText("$it\n") }

	val familiesOut = File("assets/names/polished/families.txt")
	familiesOut.writeText("")
	families.forEach {
		familiesOut.appendText(it.persons.joinToString(",") + "\n")
	}

//	println("ODDBALLS:")
//	oddballs.forEach { println(it) }
}

private fun standardNames() {
	val standard = names
			.filter { "^\\w+,".toRegex().find(it) != null }
			.map { it.replace(".", "") }

	val (multiple, single) = standard.partition { it.contains("&") }

	single
			.map { it.split(",?\\s".toRegex()) }
			.filter { it.intersect(lineBlacklist).isEmpty() }
			.mapNotNull { nameParts ->
				if (nameParts.size < 2) {
					oddballs.add(nameParts.joinToString { " " })
					null
				} else {
					val (last, first) = nameParts
					person(first, last)
				}
			}
			.filter(Person::valid)
			.toSet()
			.forEach { polished.add(it) }
//			.forEach { println(it) }


	// WALLACE, CLAUDE L & MARK WALLACE
	// VIERTEL, JESSE P IV & SHANON CORSA
	// VOLKMANN, DIETRICH H & DAWNA L VOELKL
	// TROOST, KEITH D & SANDRA K
	// TROOST, KEITH D & SANDRA
	// ENGLE, PEGGY , WILLIAM & LISA
	// COOPER, RAYMOND M & KAYCE E HYCZ-
	val blacklistLastNames = listOf("CO", "ANN", "AN")

	multiple
			.map {
				it.replace("\\s+TRUST?S?".toRegex(), "")
						.replace("\\s+ETAL".toRegex(), "")
						.replace("FAMILY", "")
			}
			.map { line ->
				try {
					val (primaryLastName, afterPrimaryLast) = line.split(",", limit = 2)
					val (primaryFirstName, afterPrimaryFirst) = afterPrimaryLast.split("&", ",", limit = 2)

					fun lastName(test: String) = if (blacklistLastNames.contains(test)) primaryLastName else test

					val secondary = afterPrimaryFirst.split("&", ",")
							.map { person ->
								val nameParts = person.trim().split(" ")
										.filter { !it.contains("(") } // is this a maiden name?
								person(nameParts[0],
										when (nameParts.size) {
											1 -> primaryLastName
											2 -> if (nameParts[1].length > 1) lastName(nameParts[1]) else primaryLastName
											else -> lastName(nameParts.last())
										})
							}

					val (valid, invalid) = listOf(person(primaryFirstName.trim().substringBefore(" "), primaryLastName)).plus(secondary)
							.partition { it.valid() }

					if (invalid.isNotEmpty())
						oddballs.add(line)

					valid
				} catch (t: Throwable) {
					println(line)
					throw t
				}
			}
			.forEach { people ->
				families.add(Family(people.toSet()))
				polished.addAll(people)
			}
//			.forEachIndexed { i, n -> println("$i $n") }
}

private fun nonstandardNames() {
	val spaces = names
			.filter { !it.contains(",") }
			.filter { it.split(" ").intersect(lineBlacklist).isEmpty() }

	spaces.forEach { println(it) }
	println(spaces.size)
}

private fun person(firstName: String, lastName: String): Person {
	fun String.uncapitalize() =
			if (this.length < 2)
				this
			else
				this[0] + this.substring(1).toLowerCase()
	return Person(firstName.uncapitalize(), lastName.uncapitalize())
}
