package com.cooper.assessments

import org.springframework.boot.autoconfigure.SpringBootApplication
import org.springframework.boot.runApplication


@SpringBootApplication
class AssessmentApiApplication

fun main(args: Array<String>) {
    runApplication<AssessmentApiApplication>(*args)
}