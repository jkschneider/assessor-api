package com.cooper.assessments

data class Person(val firstName: String, val lastName: String) {
	fun valid(): Boolean {
		val vowel = "[aeiouAEIOU]".toRegex()

		return firstName.contains(vowel) && lastName.contains(vowel) &&
				firstName.length > 1 && lastName.length > 1 &&
				!lastName.contains("[0..9]".toRegex())
	}

	override fun toString(): String = "$firstName $lastName"
}

data class Family(val persons: Set<Person>)