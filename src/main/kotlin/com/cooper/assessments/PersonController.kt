package com.cooper.assessments

import org.apache.commons.codec.language.bm.BeiderMorseEncoder
import org.apache.commons.collections4.trie.PatriciaTrie
import org.springframework.web.bind.annotation.CrossOrigin
import org.springframework.web.bind.annotation.GetMapping
import org.springframework.web.bind.annotation.RequestParam
import org.springframework.web.bind.annotation.RestController

@CrossOrigin
@RestController
class PersonController {
	companion object {
		private val encoder = BeiderMorseEncoder()
		fun soundex(name: String?): List<String> = when (name) {
			is String -> encoder.encode(name).split("|")
			else -> emptyList()
		}
	}

	val people: List<Person> = javaClass.getResourceAsStream("/names.txt")
			.reader().readLines()
			.map { it.split(" ") }
			.map { (first, last) -> Person(first, last) }

	val families: Map<Person, List<Family>> = javaClass.getResourceAsStream("/families.txt")
			.reader().readLines().filter { it.isNotBlank() }
			.map { line ->
				Family(line.split(",").map { person ->
					try {
						val (first, last) = person.split(" ")
						Person(first, last)
					} catch (t: Throwable) {
						throw t
					}
				}.toSet())
			}
			.foldMultiMapForEach { it.persons }

	// These are uppercased
	val nicknames: Map<String?, List<String>> = javaClass.getResourceAsStream("/nicknames.txt")
			.reader().readLines()
			.filter { !it.startsWith("#") }
			.fold(mutableMapOf<String?, MutableList<String>>()) { acc, nicknameLine ->
				val parts = nicknameLine.split("\\s+".toRegex())
				val (name1, name2, score) = parts
				if (score.toDouble() > 0.3) {
					acc.getOrPut(name1) { mutableListOf() }.add(name2)
					acc.getOrPut(name2) { mutableListOf() }.add(name1)
				}
				acc
			}

	val upperFirstNames: Map<String, List<Person>> = people.foldMultiMap { p -> p.firstName.toUpperCase() }
	val upperLastNames: Map<String, List<Person>> = people.foldMultiMap { p -> p.lastName.toUpperCase() }
	val firstNameSoundex: Map<String, List<Person>> = people.foldMultiMapForEach { p -> soundex(p.firstName) }
	val lastNameSoundex: Map<String, List<Person>> = people.foldMultiMapForEach { p -> soundex(p.lastName) }
	val partialFirstNames = PatriciaTrie<String>(upperFirstNames.map { (k, _) -> k to k }.toMap())
	val partialLastNames = PatriciaTrie<String>(upperLastNames.map { (k, _) -> k to k }.toMap())

	@GetMapping("/person")
	fun people(@RequestParam(required = false) q: String?): Set<Person> {
		if (q == null) return emptySet()

		val parts = q.replace(".", "")
				.split("\\s+".toRegex()).filter { it.isNotEmpty() }.take(2)

		return when (parts.size) {
			1 -> people(parts[0], null) + people(null, parts[0])
			2 -> {
				if (parts[0].endsWith(","))
					people(parts[1], parts[0].substringBefore(","))
				else people(parts[0], parts[1])
			}
			else -> emptySet()
		}
	}

	data class FamilyWithPrimary(val primary: Person, val others: Iterable<Person>)

	@GetMapping("/family")
	fun families(@RequestParam(required = false) q: String?): Set<FamilyWithPrimary> = people(q)
			.flatMapNotNull { primary ->
				families[primary]?.map { FamilyWithPrimary(primary, it.persons - primary) }
			}
			.toSet()

	fun people(firstName: String?, lastName: String?): Set<Person> {
		val firstNameMatches: List<Person> = soundex(firstName).flatMapNotNull { firstNameSoundex[it] } +
				nicknames.getOrDefault(firstName?.toUpperCase(), emptyList()).flatMapNotNull { upperFirstNames[it] } +
				partialFirstNames.prefixMap(firstName?.toUpperCase()).values.flatMapNotNull { upperFirstNames[it] }

		val lastNameMatches = soundex(lastName).mapNotNull { lastNameSoundex[it] }.flatten() +
				partialLastNames.prefixMap(lastName?.toUpperCase()).values.flatMapNotNull { upperLastNames[it] }

		return when (firstName) {
			is String -> when (lastName) {
				is String -> firstNameMatches.intersect(lastNameMatches)
				else -> firstNameMatches
			}
			else -> lastNameMatches
		}.toSet()
	}
}