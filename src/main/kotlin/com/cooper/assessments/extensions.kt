package com.cooper.assessments

fun <K, V> List<V>.foldMultiMap(key: (V) -> K): Map<K, List<V>> = fold(mutableMapOf<K, MutableList<V>>()) { acc, k ->
	acc.apply { getOrPut(key(k)) { mutableListOf() }.add(k) }
}

fun <K, V> List<V>.foldMultiMapForEach(keys: (V) -> Iterable<K>): Map<K, List<V>> = fold(mutableMapOf<K, MutableList<V>>()) { acc, k ->
	acc.apply {
		keys(k).forEach { key -> acc.getOrPut(key) { mutableListOf() }.add(k) }
	}
}

fun <T, R> Iterable<T>.flatMapNotNull(transform: (T) -> Iterable<R>?): List<R> =
		mapNotNull(transform).flatten()