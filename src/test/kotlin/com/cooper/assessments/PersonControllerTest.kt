package com.cooper.assessments

import com.cooper.assessments.PersonController.Companion.soundex
import org.assertj.core.api.Assertions.assertThat
import org.junit.jupiter.api.Test

class PersonControllerTest {
	@Test
	fun soundexTest() {
		setup@ run {}
		fun String.matchesName(name: String, matches: Boolean = true): String {
			assertThat(soundex(this).intersect(soundex(name))).let {
				if (matches) it.isNotEmpty else it.isEmpty()
			}
			return this
		}

		fun String.doesNotMatchName(name: String) = this.matchesName(name, false)

		then@ run {
			"schneider".matchesName("snyder")
					.doesNotMatchName("smith")


			"john".matchesName("jon")
					.matchesName("jean")
					.doesNotMatchName("jim")
		}
	}
}